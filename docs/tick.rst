====================
ブロックを落とす
====================

ブロックを落とす
================

（`サンプル10 <_static/index10.html>`_）

.. literalinclude:: _static/js/tetris10.js
    :language: javascript
    :linenos:
    :emphasize-lines: 1, 54-56, 59, 62

.. literalinclude:: _static/js/render10.js
    :language: javascript
    :linenos:
    :emphasize-lines: 39

発展課題
========