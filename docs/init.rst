====================
ブロックをクリアする
====================

ブロックをクリアする
==========================

（`サンプル7 <_static/index07.html>`_）

.. literalinclude:: _static/js/tetris07.js
    :language: javascript
    :linenos:

.. literalinclude:: _static/index07.html
    :language: html
    :linenos:
    :emphasize-lines: 11

発展課題
========