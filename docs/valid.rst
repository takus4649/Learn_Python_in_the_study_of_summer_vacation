================
ブロックをためる
================

下についたことを判定する
========================

（`サンプル11 <_static/index11.html>`_）

.. literalinclude:: _static/js/tetris11.js
    :language: javascript
    :linenos:
    :emphasize-lines: 55-59, 62-73

下についたブロックをためる
==========================

（`サンプル12 <_static/index12.html>`_）

.. literalinclude:: _static/js/tetris12.js
    :language: javascript
    :linenos:
    :emphasize-lines: 58, 67, 68, 77-85

発展課題
========