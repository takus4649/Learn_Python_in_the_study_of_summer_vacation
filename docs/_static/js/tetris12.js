var COLS = 10, ROWS = 20;
board = []
var interval;
var current;
var currentX, currentY

var shapes = [
    [1, 1, 1, 1],
    [1, 1, 1, 0,
     1],
    [1, 1, 1, 0,
     0, 0, 1],
    [1, 1, 0, 0,
     1, 1],
    [1, 1, 0, 0,
     0, 1, 1],
    [0, 1, 1, 0,
     1, 1],
    [0, 1, 0, 0,
     1, 1, 1],
];

function init() {
    for (var y = 0; y < ROWS; ++y) {
        board[y] = [];
        for (var x = 0; x < COLS; ++x) {
            board[y][x] = 0;
        }
    }
}

function newShape() {
    var id = Math.floor(Math.random() * shapes.length);
    var shape = shapes[id];

    current = [];
    for (var y = 0; y < 4; ++y) {
        current[y] = [];
        for (var x = 0; x < 4; ++x) {
            var i = 4 * y + x;
            if (typeof shape[i] != 'undefined' && shape[i]) {
                current[y][x] = id + 1;
            }
            else {
                current[y][x] = 0;
            }
        }
    }

    currentX = 5;
    currentY = 0;
}

function tick() {
    if(valid()) {
        ++currentY;
    } else {
        freeze();
        newShape();
    }
}

function valid() {
    for(var x = 0; x < 4; ++x) {
        for(var y = 0; y < 4; ++y) {
            if(current[y][x]) {
                if(currentY + y + 1 >= ROWS
                   || board[currentY + y + 1][currentX + x]) {
                    return false;
                }
            }
        }
    }
    return true;
}

function freeze() {
    for(var x = 0; x < 4; ++x) {
        for(var y = 0; y < 4; ++y) {
            if(current[y][x]) {
                board[y + currentY][x + currentX] = current[y][x]
            }
        }
    }
}

function newGame() {
    clearInterval(interval);
    init();
    newShape();
    interval = setInterval(tick, 250);
}

newGame();
