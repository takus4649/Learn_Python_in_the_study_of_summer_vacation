document.body.onkeydown = function(e) {
    var keys = {
        37: 'left',
        39: 'right',
        40: 'down',
        38: 'rotate',
    };

    if(typeof keys[e.keyCode] != 'undefined') {
        keyPress(keys[e.keyCode]);
        render();
    }
};

function keyPress(key) {
    switch(key) {
        case 'left':
        --currentX;
        break;
        case 'right':
        ++currentX;
        break;
        case 'down':
        ++currentY;
        break;
    }
}