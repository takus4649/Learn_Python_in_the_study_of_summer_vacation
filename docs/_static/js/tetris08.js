var COLS = 10, ROWS = 20;
board = []
var current;
var currentX, currentY

function init() {
    for (var y = 0; y < ROWS; ++y) {
        board[y] = [];
        for (var x = 0; x < COLS; ++x) {
            board[y][x] = 0;
        }
    }
}

function newShape() {
    var shape = [1, 1, 1, 0,
                 1]
    current = [];
    for (var y = 0; y < 4; ++y) {
        current[y] = [];
        for (var x = 0; x < 4; ++x) {
            var i = 4 * y + x;
            if (typeof shape[i] != 'undefined' && shape[i]) {
                current[y][x] = 1;
            }
            else {
                current[y][x] = 0;
            }
        }
    }

    currentX = 5;
    currentY = 0;
}

function newGame() {
    init();
    newShape();
}

newGame();
