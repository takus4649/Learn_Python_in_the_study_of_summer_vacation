document.body.onkeydown = function(e) {
    var keys = {
        37: 'left',
        39: 'right',
        40: 'down',
        38: 'rotate',
    };

    if(typeof keys[e.keyCode] != 'undefined') {
        keyPress(keys[e.keyCode]);
        render();
    }
};

function keyPress(key) {
    switch(key) {
        case 'left':
        --currentX;
        break;
        case 'right':
        ++currentX;
        break;
        case 'down':
        ++currentY;
        break;
        case 'rotate':
        var rotated = rotate(current);
        current = rotated;
        break;
    }
}

function rotate(current) {
    var newCurrent = [];
    for(var y = 0; y < 4; ++y) {
        newCurrent[y] = [];
        for(var x = 0; x < 4; ++x) {
            newCurrent[y][x] = current[3 - x][y];
        }
    }
    return newCurrent;
}
