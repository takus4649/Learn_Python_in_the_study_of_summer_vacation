================
JavaScriptを使う
================

JavaScriptとは
================

JavaScriptは、プログラミング言語で、ここからが本格的なプログラミングになります。
テトリスのゲームで、ブロックを表示させたり、動かしたり、ブロックがそろって消すなどの処理はすべてJavaScriptで書きます。

ブロックを描く
==============

jsファイルを作成して、1つのブロックを描きます。（`サンプル4 <_static/index04.html>`_）

1. render.jsという空のテキストファイルを作ります
2. render.jsに以下のコードを書きます。

.. literalinclude:: _static/js/render04.js
    :language: javascript
    :linenos:

3. index.htmlでrender.jsを読み込むために、以下のコードを書きます。

.. code-block:: html
    :linenos:
    :emphasize-lines: 11

    <!DOCTYPE html>
    <html>
        <head>
            <title>テトリスで遊ぼう</title>
            <link rel='stylesheet' href='style.css' />
        </head>
        <body>
            <h1>テトリス</h1>
            <p>このサイトではテトリスで遊べます。</p>
            <canvas width='300' height='600'></canvas>
            <script src='js/render.js'></script>
        </body>
    </html>

プログラムの意味
================

プログラムの意味を順に解説していきます。

.. code-block:: javascript

    var canvas = document.getElementsByTagName('canvas')[0];

**var** は、変数を指定するためのプログラムです。
変数とは、名前を付けた箱だと考えてください。
その箱には数値や文字列、オブジェクトといった色々なものを入れておくことができます。
変数に入れておくと、変数の名前で入れたものを使えるようになります。
今の場合は、 **canvas** が名前で、 **=** の右側が中に入れるものです。
=の右側のプログラムは、htmlファイルのcanvasタグを表しています。今後、canvasという変数を使って、canvasタグに変更を加えます。

.. tip::
    「var 〇〇 = ××」というプログラムは、「変数〇〇に、××を格納する」という風に説明していきます。

.. code-block:: javascript

    var ctx = canvas.getContext('2d');

変数ctxに、平面（2d）の変更を加えるためのAPI（操作するためのインタフェース）を格納しています。
今後はctxを操作することで、canvasに変更を加えることができます。
 
.. code-block:: javascript

    var COLS = 10, ROWS = 20;

変数COLSとROWSに、それぞれ10と20という数値を格納しています。
まとめて変数を指定する場合は、 **,** で区切ります。
これは、ブロックの数を横（COLS）10個、縦（ROWS）20個にすることを表しています。

.. code-block:: javascript

    var W = 300, H = 600;

変数WとHに、300と600の数値を格納しています。
これはcanvasの大きさの横300（W）と縦600（H）に合わせています。

.. code-block:: javascript

    var BLOCK_W = W / COLS, BLOCK_H = H / ROWS;

変数BLOCK_WとBLOCK_Hに、変数を使った計算式の結果を格納しています。
**/** は割り算を表します。
「W / COLS」は、「300÷10」つまり「30」を表します。
これは、ブロックの縦（BLOCK_W）、横（BLOCK_H）の大きさを表しています。
横の長さW（300）に、横のブロックの数COLS（10）を表示するには、1つのブロックの大きさはW/COLS（30）であることを表しています。

.. tip::
    四則演算（足し算、引き算、掛け算、割り算）は、それぞれ+, -, \*, /で計算できます。


.. code-block:: javascript

    function drawBlock(x, y) {
        ctx.fillRect(BLOCK_W * x, BLOCK_H * y, BLOCK_W - 1, BLOCK_H - 1);
        ctx.strokeRect(BLOCK_W * x, BLOCK_H * y, BLOCK_W - 1, BLOCK_H - 1);
    }

drawBlockという名前で、関数（function）を定義しています。
「{」と「}」で囲まれたプログラムをdrawBlockという名前で、何度でも呼び出せます。
「(x, y)」は、引数といって、関数を呼ぶときに渡したい値を設定しています。
今の場合は、xとyという二つの値を渡しています。
このdrawBlockという関数は、左からx番目、上からy番目にブロックを表示する関数です。

関数の中では、以下の処理が行われています。
ctx.fillRectは、四角を描くプログラムです。
四角の左上のx座標がBLOCK_W * x、y座標がBLOCK_H * yです。
BLOCK_W - 1が四角の横の長さを、BLOCK_H - 1が四角の縦の長さを表しています。

ctx.strokeRectは、四角の輪郭を描くプログラムです。


.. code-block:: javascript

    drawBlock(2, 5)

上記で定義した関数drawBlockを、引数x=2, y=5で呼び出しています。

chromeデベロッパーツール
========================

プログラミングをしているとエラーが発生して、うまく動かないことがあります。
そんな時は、プログラムのどこまで動いているのか、変数の値はどうなっているなどを確認できると、うまく動かない原因が特定できます。
うまく動かない部分をバグといい、そのバグを見つけることをデバッグといいます。
でバックを行う時に便利なツールである、chromeデベロッパーツールを紹介します。

chromeデベロッパーツールは、chromeのブラウザから簡単に使うことができます。
chromeでindex.htmlを表示して、その状態でメニューから「その他のツール」 => 「デベロッパーツール」を選択してください。

デベロッパーツールを起動すると、右の方に別の画面が出ます。
ここにデバッグに必要な情報が色々表示されるのですが、まずは「Console」の使い方を紹介します。
Consoleを使うことで、変数の値や関数の実行などが行えます。

Consoleに「>」というマークがついていると思いますが、そこをクリックして、「BLOCK_W」を入力し、「Enter」を押して下さい。
30という数値が表示されたと思います。これは変数BLOCK_Wには、30が格納されているということを表しています。
BLOCK_Wは計算によって、変数に値を入れていましたが、正しく入っているかどうかに使えます。

次に、「drawBlock(5, 5)」と入力し、「Enter」を押して下さい。
これで関数drawBlockが実行されて、ブロックが表示されました。

このようにデベロッパーツールでは、変数の値や関数の実行などが行え、ちゃんとプログラムが思った通りになっているかを確認できます。

発展課題
========

ブロックの数（COLSやROWS）を変えて、ブロックの表示がどのように変わるか見てみましょう。
表示させるときは、vender.jsからdrawBlockを呼ぶ方法とデベロッパーツールを使う方法、両方を試してみましょう。
試すときはどちらの方法が楽か考えてみましょう。
