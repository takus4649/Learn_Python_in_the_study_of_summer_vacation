====================
落ちるブロックを作る
====================

1種類の落ちるブロックを作る
===========================

（`サンプル8 <_static/index08.html>`_）

.. literalinclude:: _static/js/tetris08.js
    :language: javascript
    :linenos:
    :emphasize-lines: 1-3, 14-33, 35-39, 41

.. literalinclude:: _static/js/render08.js
    :language: javascript
    :linenos:
    :emphasize-lines: 25-32

ランダムに落ちるブロックを作る
==============================

（`サンプル9 <_static/index9.html>`_）

.. literalinclude:: _static/js/tetris09.js
    :language: javascript
    :linenos:
    :emphasize-lines: 5-19, 31, 32, 40

発展課題
========