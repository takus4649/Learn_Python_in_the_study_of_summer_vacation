# 目的
子どもにPythonを教えるコンテンツを作る

# 目標
子どもが夏休み中にPythonを学んで、自由研究としてWebアプリを作る

# 作成するWebアプリ
- ランキング機能付迷路ゲーム

# 学ぶ技術
- Python
- Django
- Test driven development
- git
- Heroku
- JavaScript(Vue.js)
- html
- Bootstrap
- postgresql

# 環境
- 開発環境:[codeanywhere](https://codeanywhere.com)
- 本番環境:[Heroku](https://jp.heroku.com)
- 構成管理:[Gitlab](https://gitlab.com)

# コンセプト
- アカウント作成などの登録関係は保護者が実施
- プログラミングは子ども1人で実施
- テストコードは事前に準備し提供
- テストに従って実装することで、Webアプリを作成
