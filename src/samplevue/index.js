var vm = new Vue({
    el: '#app',
    data: {
        message: 'Hello World!',
        seen: true,
        todos: [
            { text: 'Learn JavaScript' },
            { text: 'Learn Vue' },
            { text: 'Build something awesome' },
        ]
    }
})

let canvas = document.getElementById("canvas")